-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3307
-- Tiempo de generación: 25-02-2019 a las 09:24:16
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `2019_laravel`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuotas`
--

CREATE TABLE `cuotas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valor` int(11) NOT NULL,
  `concepto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha_cobro` date NOT NULL,
  `rutaFichero` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cuotas`
--

INSERT INTO `cuotas` (`id`, `nombre`, `valor`, `concepto`, `fecha_cobro`, `rutaFichero`) VALUES
(1, 'asd', 3, 'asd', '2019-02-22', 'cuota5c6e97df4afa48.36526287_ba4a513b3644cf5f42eac12e47680bb9.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario`
--

CREATE TABLE `inventario` (
  `id` int(10) UNSIGNED NOT NULL,
  `codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cantidad` mediumint(9) NOT NULL,
  `precio` double(8,2) NOT NULL,
  `disponibilidad` tinyint(1) DEFAULT NULL,
  `proveedor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ruta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `inventario`
--

INSERT INTO `inventario` (`id`, `codigo`, `nombre`, `descripcion`, `cantidad`, `precio`, `disponibilidad`, `proveedor`, `ruta`, `created_at`, `updated_at`) VALUES
(13, 'dsafas', 'asd', 'asdfasdf', 12, 12.00, NULL, 'asdfasdf', 'imagen_5c6ea41b265499.77922177ba4a513b3644cf5f42eac12e47680bb9.jpg', '2019-02-21 12:14:03', '2019-02-21 12:14:03'),
(16, '210201', 'asdasd', 'asdasd', 3, 12.00, NULL, 'dsafsghdfghdfgh', 'imagen_5c73a1e4b76500.40940407Tulips.jpg', '2019-02-25 06:59:44', '2019-02-25 07:05:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `socios`
--

CREATE TABLE `socios` (
  `id` int(10) UNSIGNED NOT NULL,
  `dni` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha_de_nacimiento` date NOT NULL,
  `localidad` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Almansa',
  `provincia` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Albacete',
  `ruta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codigo_postal` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '02640',
  `direccion` longtext COLLATE utf8_unicode_ci NOT NULL,
  `pais` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'España',
  `baja` tinyint(1) DEFAULT NULL,
  `observaciones` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `socios`
--

INSERT INTO `socios` (`id`, `dni`, `nombre`, `apellidos`, `telefono`, `fecha_de_nacimiento`, `localidad`, `provincia`, `ruta`, `codigo_postal`, `direccion`, `pais`, `baja`, `observaciones`, `created_at`, `updated_at`) VALUES
(13, 'j', 'hjbn', 'hj', 'j', '2019-02-08', 'j', 'j', 'img_5c6e8f31e2c318.16031383_depositphotos_109254752-stock-illustration-clock-icon-clock-symbol-clock.jpg', 'j', 'j', 'j', NULL, 'j,', '2019-02-04 06:14:18', '2019-02-21 10:44:49'),
(14, 'dfg', 'sdfg', 'dfg', 'dfg', '2019-02-08', 'dfhg', 'dfg', '', 'dfg', 'dfg', 'dfg', NULL, 'dfg', '2019-02-06 07:28:45', '2019-02-06 07:28:45'),
(15, 'dfhg', 'dcfb', 'df', 'dfh', '2019-02-15', 'dfbh', 'dfbh', 'img_5c656629bb1b71.47316760_6.jpg', 'dfb', 'dfb', 'dfb', NULL, 'dfb', '2019-02-06 07:32:37', '2019-02-14 10:59:21'),
(16, 'sd', 'zdsg', 'sdg', 'sdg', '2019-02-07', 'sdf', 'sdf', NULL, 'sdf', 'dsf', 'sdf', NULL, 'sdf', '2019-02-13 08:15:04', '2019-02-13 08:15:04'),
(17, 'dfg', 'wde', 'r', 'dfg', '2019-02-23', 'dfg', 'dfg', 'img_5c656a3d3ac464.11681317_ex190130.log', 'dfg', 'dfg', 'dfg', NULL, 'dfg', '2019-02-13 08:18:46', '2019-02-14 11:16:45'),
(18, 'd', 'd', 'd', 'd', '2019-02-08', 'd', 'd', '8.jpg', 'd', 'd', 'd', NULL, 'd', '2019-02-14 09:41:17', '2019-02-14 09:41:17'),
(19, 'a', 'a', 'a', 'a', '2019-02-15', 'a', 'a', '8.jpgcof_5c655a7b97cac5.01992419', 'a', 'a', 'a', NULL, 'a', '2019-02-14 10:09:31', '2019-02-14 10:09:31'),
(20, 'a', 'a', 'a', 'a', '2019-02-15', 'a', 'a', 'img_5c668a13cdb147.67784246_9.jpg', 'a', 'a', 'a', NULL, 'fg', '2019-02-14 10:10:54', '2019-02-15 07:44:51'),
(21, 'a', 'a', 'a', 'a', '2019-02-21', 'a', 'a', 'img_5c6e92103fd4d1.29633559_ba4a513b3644cf5f42eac12e47680bb9.jpg', 'a', 'a', 'a', NULL, 'a', '2019-02-15 07:46:25', '2019-02-21 10:57:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tesoreria`
--

CREATE TABLE `tesoreria` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` int(11) NOT NULL,
  `fecha` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `precio` double(8,2) NOT NULL,
  `fotoFactura` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tesoreria`
--

INSERT INTO `tesoreria` (`id`, `nombre`, `telefono`, `fecha`, `description`, `precio`, `fotoFactura`) VALUES
(46, 'Jorge', 987547245, '2019-01-11', 'Compra de la bebida', 80.00, 'ruta'),
(47, 'dani', 52222, '2019-02-02', 'hola', 23.00, '12.jpg'),
(48, 'maria', 23456789, '2019-02-14', 'foto prueba', 23.00, 'cof_5c6562cbd7d9c3.29993874_1.jpg'),
(49, 'dani', 234566, '2019-02-07', 'prueba 2', 23.00, 'cof_5c65662d11c526.37293696_9.jpg'),
(50, 'Roberto', 655675342, '2019-02-05', 'Pago botas', 53.00, 'cof_5c659f7db75949.10214377_2.jpg'),
(51, 'manuel', 234556, '2019-02-06', 'no funciona nada', 23.00, 'cof_5c66acc2331405.64862730_php.net.txt'),
(52, 'paco', 6232, '2019-02-12', 'vaya puta mierda', 23.00, 'cof_5c66ad1f9a8064.49601513_php.net.txt'),
(53, 'asd', 12345, '2019-02-13', 'asdasd', 12.00, 'cof_5c6e95c582c086.57334179_ba4a513b3644cf5f42eac12e47680bb9.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'daw', 'daw@gmail.com', NULL, '$2y$10$UUz0xV0O.G5J2ySf1UDvPu1Epz8hnErR.sEi0AkvEVFxdy7XcFCgK', 'UK4kCAY08CZogYPAutJpfV4CTeNshqXKAPK8oiMEsxnYvHvWnN7r7zSxuPTu', '2019-02-06 08:59:30', '2019-02-06 08:59:30');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cuotas`
--
ALTER TABLE `cuotas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `socios`
--
ALTER TABLE `socios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tesoreria`
--
ALTER TABLE `tesoreria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cuotas`
--
ALTER TABLE `cuotas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `inventario`
--
ALTER TABLE `inventario`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `socios`
--
ALTER TABLE `socios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `tesoreria`
--
ALTER TABLE `tesoreria`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

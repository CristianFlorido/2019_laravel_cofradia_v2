<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tesoreria;
use Illuminate\Support\Facades\Storage;

class TesoreriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
		$listadoTesoreria = Tesoreria::all();
		return view('tesoreria.index' , ['listadoTesoreria'=> $listadoTesoreria]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('tesoreria.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'nombre'=>'required',
            'precio'=>'required',
            'imagen' => 'mimes:jpg,jpeg,png,gif'
        ]);
        /*
         * $post = Post::create($request->all();
         if($request->file('file')){
            $path = Storage::disk('public')->put('image', $request->file('file'));
            $post->fill(['file' => asset($path)])->save());
        }*/

        $fichero = $request->file('imagen');

        $nombre_fichero=$fichero->getClientOriginalName();
        $path = 'storage/fotos_factura';
        $nombre_fichero = uniqid('cof_',true).'_'.$nombre_fichero;
        $request->file('imagen')->move($path,$nombre_fichero);


        $tesoreria = new Tesoreria ([
            'nombre' => $request->get('nombre'),
            'telefono' => $request->get('telefono'),
            'fecha' => $request->get('fecha'),
            'description' => $request->get('description'),
            'precio' => $request->get('precio'),
            'fotoFactura'=> $nombre_fichero,
        ]);
        $tesoreria->save();
        return redirect('/tesoreria')->with('success', 'Se ha añadido correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tesoreria = Tesoreria::find($id);

        return view('tesoreria.show', compact ('tesoreria'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tesoreria = Tesoreria::find($id);

        return view('tesoreria.edit', compact ('tesoreria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'nombre'=>'required',
            'precio'=>'required',
            'imagen' => 'mimes:jpg,jpeg,png,gif'
        ]);

        $fichero = $request->file('imagen');

        $nombre_fichero=$fichero->getClientOriginalName();
        $path = 'storage/fotos_factura';
        $nombre_fichero = uniqid('cof_',true).'_'.$nombre_fichero;
        $request->file('imagen')->move($path,$nombre_fichero);


        $tesoreria = Tesoreria::find($id);
            $tesoreria -> nombre = $request->get('nombre');
            $tesoreria -> telefono = $request->get('telefono');
            $tesoreria -> fecha = $request->get('fecha');
            $tesoreria -> description = $request->get('description');
            $tesoreria -> precio = $request->get('precio');
            $tesoreria -> fotoFactura = $nombre_fichero;

        $tesoreria->save();
        return redirect('/tesoreria')->with('success', 'Se ha añadido correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tesoreria = Tesoreria::find($id);
        $tesoreria -> delete();

        return redirect('/tesoreria')->with('success', 'Se ha eliminado correctamente');
    }
}

<!DOCTYPE html>
<html>
<head>
	<title>Crear Inventario</title>


<!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
	<div class="container">
		 <nav>
		    <div class="nav-wrapper">
		      <a href="#" class="brand-logo">Crear Inventario</a>
		      <ul id="nav-mobile" class="right hide-on-med-and-down">
		        <li><a href="{{ route('inventario.index') }}">Volver a Listado</a></li>
		      </ul>
		    </div>
		  </nav>
		 @if ($errors->any())
	      <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	              <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	      </div><br />
	    @endif
		<form method="post" action="{{ route('inventario.store') }}" enctype="multipart/form-data">
			 @csrf
		<div class="input-field col s12">
          <input id="codigo" name="codigo" type="text" class="validate">
          <label for="last_name">Código</label>
        </div>
		  <div class="row">
		    <div class="col s12">
		      <div class="row">
		        <div class="input-field col s12">
		          
		          <input type="text" id="nombre" name="nombre" class="autocomplete">
		          <label for="autocomplete-input">Nombre</label>
		        </div>
		      </div>
		    </div>
		  </div>
		<div class="row">
          <div class="input-field col s12">
            <input id="descripcion" name="descripcion" type="text" data-length="120">
            <label for="input_text">Descripción</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input id="cantidad" name="cantidad" type="number" >
            <label for="input_text">Cantidad</label>
          </div>
        </div>
		  
		 <div class="row">
          <div class="input-field col s12">
            <input id="precio" name="precio" type="number" >
            <label for="input_text">Precio  <i class="material-icons right">euro_symbol</i></label>
          </div>
        <div class="row">
          <div class="input-field col s12">
            <input id="proveedor" name="proveedor" type="text" >
            <label for="input_text">Proveedor</label>
          </div>
        </div>
        <div class="row">
		   <div class="file-field input-field">
		      <div class="btn">
		        <span>File</span>
		        <input type="file" name="fichero" id="fichero">
		      </div>
		      <div class="file-path-wrapper">
		        <input class="file-path validate" type="text">
		      </div>
		    </div>
		 </div>
		 <!-- <div class="form-group">
		  	<input type="file" name="fichero" id="fichero">
		  </div>-->
		  <div class="center-align">
		   <button class="btn waves-effect waves-light " type="submit" name="action">Guardar
		    <i class="material-icons right">cloud</i>
		  </button>
		  </div>
		  <!-- <button type="submit" class="btn btn-primary offset-5 mb-2">Guardar</button>-->
		</form>
		 <div class="row">
		 </div>
		  <footer class="page-footer ">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Footer Crear</h5>
                <p class="grey-text text-lighten-4">Footer Creado con Materialize.</p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Links</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="{{ route('inventario.index') }}">Inventario</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2019 Copyright Text
            <a class="grey-text text-lighten-4 right" href="#">Más Links</a>
            </div>
          </div>
        </footer>
	</div>
	 
	 <script type="text/javascript">
	 	 document.addEventListener('DOMContentLoaded', function() {
		    var elems = document.querySelectorAll('.autocomplete');
		    var instances = M.Autocomplete.init(elems, options);
		  });


  // Or with jQuery

		$(document).ready(function(){
			$('input.autocomplete').autocomplete({
			  data: {
			    "Báculo": null,
			    "Tambor": null,
			    "Túnica": null
			  },
			});
		});

	 </script>
	 <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</body>
</html>

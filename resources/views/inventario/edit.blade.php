@extends('layout.layout')
@section('content')
	<div class="container">
		 @if ($errors->any())
	      <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	              <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	      </div><br />
	    @endif
	    @if(session()->get('status'))
		    <div class="alert alert-danger">
		      {{ session()->get('status') }}  
		    </div><br />
		@endif
		<form method="post" action="{{ route('inventario.update', $inventario->id) }}" enctype="multipart/form-data">
			@method('PUT')
			 @csrf
		  <div class="form-group">
		    <label for="formGroupExampleInput">Código</label>
		    <input type="text" class="form-control" id="codigo" name="codigo" value="{{$inventario->codigo}}">
		  </div>
		  <div class="form-group">
		    <label for="formGroupExampleInput2">Nombre</label>
		    <input type="text" class="form-control" id="nombre" name="nombre" value="{{$inventario->nombre}}">
		  </div>
		  <div class="form-group">
		    <label for="exampleFormControlTextarea1">Descripción</label>
		    <textarea class="form-control" id="descripcion" name="descripcion" rows="3" value="{{$inventario->descripcion}}"></textarea>
		  </div>
		  <div class="form-group">
		    <label for="formGroupExampleInput2">Cantidad</label>
		    <input type="text" class="form-control" id="cantidad" name="cantidad" value="{{$inventario->cantidad}}">
		  </div>
		  <div class="form-group">
		    <label for="formGroupExampleInput2">Precio</label>
		    <input type="text" class="form-control" id="precio" name="precio" value="{{$inventario->precio}}">
		  </div>
		  <div class="form-group">
		    <label for="formGroupExampleInput2">Proveedor</label>
		    <input type="text" class="form-control" id="proveedor" name="proveedor" value="{{$inventario->proveedor}}">
		  </div>
		  <div class="form-group">
		  	@if(!empty($inventario->ruta))
							<p><mark>IMAGEN</mark>:
							<div class="card" style="width: 18rem;">
							  <img class="card-img-top" src="{{ asset ('storage/'.$inventario->ruta) }}" alt="Card image cap">
							</div></p>
						@endif
						<div class="input-group mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text" id="inputGroupFileAddon01">Subir</span>
						  </div>
						  <div class="custom-file">
						    <input type="file" name="fichero" id="fichero" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
						    <label class="custom-file-label" for="inputGroupFile01">Elige imagen</label>
						  </div>
						</div>
		  </div>
		  <button type="submit" class="btn btn-primary offset-5 mb-2">Actualizar</button>
		</form>
	</div>
@endsection
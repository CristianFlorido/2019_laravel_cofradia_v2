<!DOCTYPE html>
<html>
<head>
	<title>inventario</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</head>
<body>
	
		<header>
		      <div class="collapse bg-dark" id="navbarHeader">
		        <div class="container">
		          <div class="row">
		            <div class="col-sm-8 col-md-7 py-4">
		              <h4 class="text-white">Sobre nosotros</h4>
		              <p class="text-muted">Add some information about the album below, the author, or any other background context. Make it a few sentences long so folks can pick up some informative tidbits. Then, link them off to some social networking sites or contact information.</p>
		            </div>
		            <div class="col-sm-4 offset-md-1 py-4">
		              <h4 class="text-white">Secciones</h4>
		              <ul class="list-unstyled">
		                <li><a href="{{ route('inventario.index') }}" class="text-white">Inventario</a></li>
                      <li><a href='{{ route("socios.index") }}' class="text-white">Socios</a></li>
                        <li><a href="#" class="text-white">Prestamos</a></li>
                        <li><a href='{{ route("tesoreria.index") }}' class="text-white">Tesoreria</a></li>
                        <li><a href='{{ route("cuotas.index") }}' class="text-white">Cuota</a></li>
                        <li><a href="{{ route('inventario.create') }}" class="text-white">Crear Inventario</a></li>
                        <li><a href="#" class="text-white">Email</a></li>
                         @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Usuario: {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu " aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
		              </ul>
		            </div>
		          </div>
		        </div>
		      </div>
		      <div class="navbar navbar-dark bg-dark shadow-sm">
		        <div class="container d-flex justify-content-between">
		          <a href="#" class="navbar-brand d-flex align-items-center">
		            <img src="{{ asset('imagenes/rocio-icono.jpg') }}" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="mr-2"></img>
		            <strong>Cofradia</strong>
		          </a>
		          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
		            <span class="navbar-toggler-icon"></span>
		          </button>
		        </div>
		      </div>
		    
		 </header>
	
	<div class="container"> 
		 @yield('content')
	</div>

	<!-- Footer -->
<footer class="page-footer font-small cyan darken-3 ">

    <!-- Footer Elements -->
    <div class="container">

      <!-- Grid row-->
      <div class="row">

        <!-- Grid column -->
        <div class="col-md-3 py-1 offset-md-5">
          <div class="mb-1 flex-center">

            <!-- Facebook -->
           <a href="#" class="fa fa-facebook"></a>
			<a href="#" class="fa fa-twitter"></a>
			<a href="#" class="fa fa-google"></a>
			
			<a href="#" class="fa fa-youtube"></a>
			<a href="#" class="fa fa-instagram"></a>
			<a href="#" class="fa fa-pinterest"></a>
			
			<a href="#" class="fa fa-reddit"></a>
			
            </a>
          </div>
        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row-->

    </div>
    <!-- Footer Elements -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2018 Copyright:
      <a href="https://mdbootstrap.com/education/bootstrap/"> MDBootstrap.com</a>
    </div>
    <!-- Copyright -->

  </footer>
  <!-- Footer -->
</body>
</html>
	
	@extends('layouts.layout')

	@section('content')

	@if(session()->get('success'))
		<div>
		{{ session()->get('success') }}
		</div>
	@endif
	<div>
		<a class="btn btn-success" href="{{ route('tesoreria.create') }}">Añadir</a>
		<table class="table table-hover"">
			<thead class="thead-dark">
				<th scope="col">Nombre</th>
				<th scope="col">Descripcion</th>
				<th scope="col">Precio</th>
				<th scope="col">Fecha</th>
				<th scope="col">Ver</th>
				<th scope="col">Editar</th>
				<th scope="col">Eliminar<th>
			</thead>
			<tbody>
				<?php foreach($listadoTesoreria as $x){?>
				
				<tr>
					<td><?php echo $x->nombre ?></td>
					<td><?php echo $x->description ?></td>
					<td><?php echo $x->precio ?></td>
					<td><?php echo $x->fecha ?></td>
					<td><a class="btn btn-primary" href="tesoreria/show/<?php echo $x->id ?>">Ver</a></td>
					<td><a class="btn btn-info" href="tesoreria/edit/<?php echo $x->id ?>">Editar</a></td>
					<td><form method="post" action="{{ route('tesoreria.destroy' , $x->id) }}">
						@METHOD('DELETE')
						@csrf
						<button class="btn btn-danger">Eliminar</button>
						</form>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	@endsection